//write data up firebase
function writeData() {
	firebase.database().ref("user").set({
	   title: document.getElementById("title").value,
	   content: document.getElementById("content").value
	});	
}	

// read data from firebase
function readData() {
	firebase.database().ref("user/title").on("value", function(snapshot) {
		console.log(snapshot.val());
	}, function (error) {
		console.log("Error: " + error.code);
	});
}  